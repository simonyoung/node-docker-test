# README #

### Prerequisites: ###

* Docker running on your local machine

### Clone the repo and build the Docker container from the included Docker image: ###

```
git clone git@bitbucket.org:simonyoung/node-docker-test.git node-docker-test
cd node-docker-test
docker build -t simonyoung/centos-node-hello .
```

### Confirm the Docker container build was successful: ###

```
docker images
```

You should see simonyoung/centos-node-hello and centos (the base box)

### Set a container running: ###

```
docker run -p 49160:8080 -d simonyoung/centos-node-hello
```

### Access your NodeJS app now running inside a Docker container: ###

You just started a Docker container on your local port 49160 and mapped it to port 8080 inside your container.  Check it's there:


```
docker ps

```

You should see a running container.  Now you can access it in your browser or via curl:

```
curl http://localhost:49160
```